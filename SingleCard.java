public class SingleCard{
	private Color colors;
	private Value values;
	
	public SingleCard(Color colors, Value values)
	{
		this.colors = colors;
		this.values = values;
	}
	
	public Color getColor()
	{
		return this.colors;
	}
	
	public Value getValue()
	{
		return this.values;
	}
	
	public String toString()
	{	
		return "++++++++++++++++"+"\n"+"+ " +this.colors+ "          +" + "\n"+ "+              +"+ "\n"+ "+              +"+ "\n"+ "+              +"
		+ "\n"+ "+       "+this.values.getCardValue()+"      +"+ "\n" + "+              +" + "\n"+ "+              +"+ "\n"+ 
		"+              +"+ "\n"+ "+          "+this.colors+" +"+"\n"+"++++++++++++++++";
	}
}