public class DynamicCardArray {
	private SingleCard[] cardValues;
	private int next;
	
	public DynamicCardArray() {
		this.cardValues = new SingleCard[1000];
		this.next = 0;
	}
	
	public void add(SingleCard card)
	{
		if(this.next >= this.cardValues.length)
		{
			this.doubleCapacity();
		}
		this.cardValues[this.next] = card;
		this.next++;
	}
	
	public void drawCard(SingleCard card, int i)
	{
		for(int y = this.next; y > i; y--)
		{
			this.cardValues[y] = this.cardValues[y-1];
		}
		this.cardValues[i] = card;
		this.next++;
	}
	
	public void playCard(int i)
	{
		if(this.next >= this.cardValues.length)
		{
			this.doubleCapacity();
		}
		for(int y=i; y<this.cardValues.length; y++)
		{
			this.cardValues[y] = this.cardValues[y+1];
		}
		this.next--;
	}
	
	public int length()
	{
		return this.next;
	}
	
	public SingleCard getCard(int index)
	{
		return this.cardValues[index];
	}
	
	//doubleCapacity() -- increases the size of this.cardValues
	private void doubleCapacity()
	{
		SingleCard[] bigger = new SingleCard[cardValues.length*2];
	
		for(int i=0; i<this.cardValues.length; i++)
		{
			bigger[i] = this.cardValues[i];
		}
		this.cardValues = bigger;
	}
	
	public String toString(){
		String s = "";
		for(int i=0; i<this.next; i++)
		{
			s += cardValues[i] + "\n";
		}
		return s;
	}
}