public enum Value{
	ZERO (0),
	ONE (1),
	TWO (2),
	THREE (3),
	FOUR (4),
	FIVE (5),
	SIX (6),
	SEVEN (7),
	EIGHT (8),
	NINE (9);
	//DRAW_TWO (+2);

	private final int cardValue;
	
	private Value(final int cardValue) 
	{
		this.cardValue = cardValue;
	}
	public int getCardValue() 
	{
		return this.cardValue;
	}
}
