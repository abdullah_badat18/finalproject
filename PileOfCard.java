public class PileOfCard{
	private DynamicCardArray pile;
	
	public PileOfCard ()
	{
		this.pile = new DynamicCardArray();
	}

	public String toString()
	{	
		return "" + this.pile.getCard(this.pile.length() - 1);
	}
	
	public void setNewCardToPlay(SingleCard oneCard)
	{
		this.pile.add(oneCard);
	}

}